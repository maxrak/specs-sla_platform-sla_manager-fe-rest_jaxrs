<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>REST Client</title>
<script type="text/javascript">

 	var api_path = "sla_manager_rest_api/";
 	var apis = ["slas","csp_sla"];

	function call(method) {
	var xmlhttp;
	var url = api_path+document.getElementById('apis').value;
	
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

/* 		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("result").innerHTML = xmlhttp.responseText;
			}
		} */
		
		
		xmlhttp.open(method, url, false);
		xmlhttp.send();
		document.getElementById("head").innerHTML =xmlhttp.getAllResponseHeaders();
		document.getElementById("result").innerHTML =xmlhttp.responseText;
		document.getElementById("status").innerHTML =xmlhttp.status;
		document.getElementById("statustxt").innerHTML =xmlhttp.statusText;
		
	}
</script>

</head>
<body>

<script language="javascript">
document.write (api_path);
</script>
<!-- <select id="apis">
</select>
<script language="javascript">
/*var select = document.getElementById("apis"); 
for(var i = 0; i < apis.length; i++) {
 
    var el = document.createElement("option");
    el.textContent = apis[i];
    el.value = apis[i];
    select.appendChild(el);*/
}
</script> -->
<input type="text" id="apis"/>


<table>
<tr>
	<td><button onclick="call('GET')">GET</button></td>
	<td><button onclick="call('POST')">POST</button></td>
</tr>
<tr>
	<td><button onclick="call('PUT')">PUT</button></td>
	<td><button onclick="call('DELETE')">DELETE</button></td>
</tr>
<tr>
	<td><button onclick="call('HEAD')">HEAD</button></td>
	<td><button onclick="call('OPTIONS')">OPTIONS</button></td>
</tr>
<tr>
	<td><button onclick="call('PATCH')">PATCH</button></td>
</tr>
</table>

<h2>HEADERS</h2>
<div id="status"></div>
<div id="statustxt"></div>
<div id="head"></div>

<h2>RESULT</h2>
<div id="result"></div>


</body>
</html>