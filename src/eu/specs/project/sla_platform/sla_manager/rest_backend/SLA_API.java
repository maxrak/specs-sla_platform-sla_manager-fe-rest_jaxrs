package eu.specs.project.sla_platform.sla_manager.rest_backend;

import eu.specs.project.sla_platform.sla_manager.api.SLA_Manager;
import eu.specs.project.sla_platform.sla_manager.api.SLA_Manager_Factory;

public class SLA_API {
	
	private static SLA_API instance = null;
	private SLA_Manager managerEU;
	
	private SLA_API(){
		
		managerEU = SLA_Manager_Factory.get_SLA_Manager_Instance("mysql");
	}
	
	
	public static SLA_API getInstance() {
		if (instance==null)
			instance = new SLA_API();
		
		return instance;
	}
	
	public SLA_Manager getManager(){
		return managerEU;
	}
	

}
