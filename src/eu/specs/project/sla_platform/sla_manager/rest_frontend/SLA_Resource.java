package eu.specs.project.sla_platform.sla_manager.rest_frontend;

import java.net.URI;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import eu.specs.project.sla_platform.sla_manager.api.entities.SLA_Document;
import eu.specs.project.sla_platform.sla_manager.internal.SLA_Identifier;
import eu.specs.project.sla_platform.sla_manager.rest_backend.SLA_API;

@Path("/slas")
public class SLA_Resource {
	
	@Context
	UriInfo uriInfo; 
	
	  @GET
	 // @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({  MediaType.APPLICATION_JSON })
	  public String getSLAS() {

		  //TODO criterias
		  List <SLA_Identifier> list = SLA_API.getInstance().getManager().search(null);
		 
		  String jsonArrayRes = "[";
		  
		  for (SLA_Identifier id : list){ 
	            
			//  JSONArray uriArray = new JSONArray();
			  UriBuilder ub = uriInfo.getAbsolutePathBuilder();
	          URI slaURI = ub.
	                    path(id.toString()).
	                    build();
	          jsonArrayRes += "\"" + slaURI.toASCIIString()+"\",";
		  }
		  jsonArrayRes = jsonArrayRes.substring(0, jsonArrayRes.length()-1);
		  return jsonArrayRes+"]";
	  }
	
	  @Path("/{id}")
	  @GET
	  public String getSLA(@PathParam("id") String id_string){
		  
		  SLA_Identifier id = new SLA_Identifier(Integer.valueOf(id_string));
		  
		  return SLA_API.getInstance().getManager().retrieve(id).toString();
	  }
	  
	  
	  @Path("/{id}/status")
	  @GET
	  public String getSLAstatus(@PathParam("id") String id_string){
		  
		  SLA_Identifier id = new SLA_Identifier(Integer.valueOf(id_string));
		  
		  return SLA_API.getInstance().getManager().getState(id).toString();
	  }
	  
	  
	  @POST
	  public Response createSLA(){
		  
		  SLA_Identifier id = SLA_API.getInstance().getManager().create(new SLA_Document()); 
		  UriBuilder ub = uriInfo.getAbsolutePathBuilder();
		  URI slaURI = ub.
                  path(id.toString()).
                  build();
		  return Response.created(slaURI).build();
	  }

	  
	 
	

}
